EVENT_INT, EVENT_FLOAT, EVENT_STRING = "INT", "FLOAT", "STR"


class EventGet:
    def __init__(self, value):
        self.kind = {int: EVENT_INT, float: EVENT_FLOAT, str: EVENT_STRING}[value]
        self.value = None
        
        
class EventSet:
    def __init__(self, value):
        self.kind = {int: EVENT_INT, float: EVENT_FLOAT, str: EVENT_STRING}[type(value)]        
        self.value = value

        
class NullHandler:
    def __init__(self, successor=None):
        self.__successor = successor

    def handle(self, obj, event):
        if self.__successor is not None:
            return self.__successor.handle(obj, event)
            
            
class IntHandler(NullHandler):
    def handle(self, obj, event):
        if event.kind == EVENT_INT:
            if event.value is None:
                return obj.integer_field
            else:
                obj.integer_field = event.value
        else:
            return super().handle(obj, event)
            

class FloatHandler(NullHandler):
    def handle(self, obj, event):
        if event.kind == EVENT_FLOAT:
            if event.value is None:
                return obj.float_field
            else:
                obj.float_field = event.value
        else:
            return super().handle(obj, event)
            

class StrHandler(NullHandler):
    def handle(self, obj, event):
        if event.kind == EVENT_STRING:
            if event.value is None:
                return obj.string_field
            else:
                obj.string_field = event.value
        else:
            return super().handle(obj, event)