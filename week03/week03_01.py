from abc import ABC, abstractmethod


class Hero:
    def __init__(self):
        self.positive_effects = []
        self.negative_effects = []
        self.stats = {
            "HP": 128,  # health points
            "MP": 42,  # magic points, 
            "SP": 100,  # skill points
            "Strength": 15,  # сила
            "Perception": 4,  # восприятие
            "Endurance": 8,  # выносливость
            "Charisma": 2,  # харизма
            "Intelligence": 3,  # интеллект
            "Agility": 8,  # ловкость 
            "Luck": 1  # удача
        }

    def get_positive_effects(self):
        return self.positive_effects.copy()

    def get_negative_effects(self):
        return self.negative_effects.copy()

    def get_stats(self):
        return self.stats.copy()

 
class AbstractEffect(Hero, ABC):
    def __init__(self, base):
        self.base = base
    
    @abstractmethod    
    def get_positive_effects(self):
        pass

    @abstractmethod 
    def get_negative_effects(self):
        pass

    @abstractmethod 
    def get_stats(self):
        pass

 
class AbstractPositive(AbstractEffect):
    def get_negative_effects(self):
        return self.base.get_negative_effects()

        
class AbstractNegative(AbstractEffect):
    def get_positive_effects(self):
        return self.base.get_positive_effects()


class Berserk(AbstractPositive):
    def get_stats(self):
        eff_stats = self.base.get_stats()
        eff_stats['Strength'] += 7
        eff_stats['Endurance'] += 7
        eff_stats['Agility'] += 7
        eff_stats['Luck'] += 7
        eff_stats['Perception'] -= 3
        eff_stats['Charisma'] -= 3
        eff_stats['Intelligence'] -= 3
        eff_stats['HP'] += 50
        return eff_stats

    def get_positive_effects(self):
        effect_list = self.base.get_positive_effects()
        if effect_list == None:
            effect_list = []
        effect_list.append('Berserk')
        return effect_list


class Blessing(AbstractPositive):
    def get_stats(self):
        eff_stats = self.base.get_stats()
        eff_stats['Strength'] += 2
        eff_stats['Endurance'] += 2
        eff_stats['Agility'] += 2
        eff_stats['Luck'] += 2
        eff_stats['Perception'] += 2
        eff_stats['Charisma'] += 2
        eff_stats['Intelligence'] += 2
        return eff_stats
        
    def get_positive_effects(self):
        effect_list = self.base.get_positive_effects()
        if effect_list == None:
            effect_list = []
        effect_list.append('Blessing')
        return effect_list


class Weakness(AbstractNegative):
    def get_stats(self):
        eff_stats = self.base.get_stats()
        eff_stats['Strength'] -= 4
        eff_stats['Endurance'] -= 4
        eff_stats['Agility'] -= 4
        return eff_stats
        
    def get_negative_effects(self):
        effect_list = self.base.get_negative_effects()
        if effect_list == None:
            effect_list = []
        effect_list.append('Weakness')
        return effect_list


class Curse(AbstractNegative):
    def get_stats(self):
        eff_stats = self.base.get_stats()
        eff_stats['Strength'] -= 2
        eff_stats['Endurance'] -= 2
        eff_stats['Agility'] -= 2
        eff_stats['Luck'] -= 2
        eff_stats['Perception'] -= 2
        eff_stats['Charisma'] -= 2
        eff_stats['Intelligence'] -= 2
        return eff_stats
        
    def get_negative_effects(self):
        effect_list = self.base.get_negative_effects()
        if effect_list == None:
            effect_list = []
        effect_list.append('Curse')
        return effect_list


class EvilEye(AbstractNegative):
    def get_stats(self):
        eff_stats = self.base.get_stats()
        eff_stats['Luck'] -= 10
        return eff_stats
        
    def get_negative_effects(self):
        effect_list = self.base.get_negative_effects()
        if effect_list == None:
            effect_list = []
        effect_list.append('EvilEye')
        return effect_list
