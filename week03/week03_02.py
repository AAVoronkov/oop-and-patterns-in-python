class MappingAdapter:
    def __init__(self, adaptee):
        self.adaptee = adaptee

    def lighten(self, grid):
        dim = (len(grid[0]), len(grid))
        self.adaptee.set_dim(dim)
        lights_list, obs_list = [], []
        for line in range(len(grid)):
            for unit in range(len(grid[0])):
                if grid[line][unit] == 1:
                    lights_list.append((unit, line))
                elif grid[line][unit] == -1:
                    obs_list.append((unit, line))
        self.adaptee.set_lights(lights_list)
        self.adaptee.set_obstacles(obs_list)
        return self.adaptee.generate_lights()